import sys
import pika

# Connecting with RabbitMQ
connection = pika.BlockingConnection(
	pika.ConnectionParameters("localhost")
)

# Creating a channel
channel = connection.channel()

# Creating a exchange
channel.exchange_declare(
	exchange='paderia', 
	exchange_type='direct',
)

# Creating a queue in the channel
queue = channel.queue_declare(queue='')
queue_name = queue.method.queue

pao_types = sys.argv[1:]

if not pao_types:
	raise ValueError("Please defined what pao type will receive. `python consumer.py baguette pao_carteira")

print(f"Waiting for {pao_types}")
for pao_type in pao_types:
	channel.queue_bind(
		exchange='paderia', 
		queue=queue_name,
		routing_key=pao_type,
	)

def callback(ch, method, properties, body):
	print(f"MSG RECEIVED: {body}", method.routing_key)


# Creating basic consumer
channel.basic_consume(
	queue=queue_name,
	auto_ack=True,
	on_message_callback=callback,
)

print("Start consuming")
# Start consuming
try:
	channel.start_consuming()
finally:
	connection.close()
