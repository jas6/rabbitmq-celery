import pika
from pprint import pprint

# Connecting with RabbitMQ
connection = pika.BlockingConnection(
	pika.ConnectionParameters("localhost")
)

# Creating a channel
channel = connection.channel()


# channel.exchange_delete(exchange='paderia')
# Creating a exchange
channel.exchange_declare(
	exchange='paderia', 
	exchange_type='direct',
)


pao_type = (
	'pao_frances',
	'pao_de_leite',
	'baguette',
	'pao_carteira',
)

try:
	while True:
		pprint(pao_type)
		_pao = int(input("Que tipo de pao voce vai a enviar [0-3]: "))

		if _pao < 0:
			break

		_type = pao_type[_pao]

		print(f"SENDING {_type}")

		# Sending a message
		channel.basic_publish(
			exchange='paderia',
			routing_key=_type,
			body=f"Sending pao: {_type}",
		)

	print("Message Finised")
finally:
	# Closing the connection with rabbitmq
	connection.close()






