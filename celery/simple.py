from celery import Celery

app = Celery(
    'simple', 
    broker='pyamqp://guest:guest@localhost//',
    backend='db+sqlite:///task.db',
    result_extended=True
)


@app.task
def add(x, y):
    """ SUM celery task """
    return x + y


@app.task(queue='instagram_facebook')
def big_tasks(message):
    print(f"MESSAGE: {message}")
    return 50


@app.task(queue='twitter_reddit')
def instagram_bot():
    # BOT CODE
    return 50


instagram_bot.delay(data)