import pika

# Connecting with RabbitMQ
connection = pika.BlockingConnection(
	pika.ConnectionParameters("localhost")
)

# Creating a channel
channel = connection.channel()

# Creating a queue in the channel
channel.queue_declare(queue='hello')


# Sending a message
channel.basic_publish(
	exchange='',
	routing_key='hello',
	body='Minha primeira mensagem com rabbitmq',
)

print("Message Sended")

# Closing the connection with rabbitmq
connection.close()
