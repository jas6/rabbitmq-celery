import pika

# Connecting with RabbitMQ
connection = pika.BlockingConnection(
	pika.ConnectionParameters("localhost")
)

# Creating a channel
channel = connection.channel()

# Creating a queue in the channel
channel.queue_declare(queue='hello')


def callback(ch, method, properties, body):
	print(f"MSG RECEIVED: {body}")


# Creating basic consumer
channel.basic_consume(
	queue='hello',
	auto_ack=True,
	on_message_callback=callback,
)

print("Start consuming")
# Start consuming
try:
	channel.start_consuming()
finally:
	connection.close()
